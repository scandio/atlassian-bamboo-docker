#!/bin/sh

env 
if [ $RAM_MIN -gt 0 ] && [ $RAM_MAX -gt 0 ]; then
        sed -i "s|^JVM_MINIMUM_MEMORY=\"[[:digit:]]*m\"$|JVM_MINIMUM_MEMORY=\"${RAM_MIN}m\"|g" ${BAMBOO_INSTALL}/bin/setenv.sh
        sed -i "s|^JVM_MAXIMUM_MEMORY=\"[[:digit:]]*m\"$|JVM_MAXIMUM_MEMORY=\"${RAM_MAX}m\"|g" ${BAMBOO_INSTALL}/bin/setenv.sh
fi

# Fetch backup data over the network
if [ "${BACKUP_HOST}" != "false" ] && [ ! -f ${BAMBOO_HOME}/DONTSYNC ]; then
	SSH_KEY_FILE_BACKUP="/tmp/key.backup"
	touch ${SSH_KEY_FILE_BACKUP}
	chmod 600 ${SSH_KEY_FILE_BACKUP}
	printf -- "${BACKUP_SSHKEY}" > ${SSH_KEY_FILE_BACKUP}

	rsync -arcz -e "ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${SSH_KEY_FILE_BACKUP}" ${BACKUP_USER}@${BACKUP_HOST}:${BACKUP_PATH}/* ${BAMBOO_HOME}
	touch ${BAMBOO_HOME}/DONTSYNC

	rm ${SSH_KEY_FILE_BACKUP}

	echo "Synchronized backup data over the network from ${BACKUP_HOST}"
fi

if [ "$BAMBOO_REMOTE_DEBUG" = "true" ]; then
	echo "set remote debugging port"
	CATALINA_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,address=$DEBUG_PORT,server=y,suspend=n $CATALINA_OPTS"
fi

if [ "$PROXY_NAME" != "false" ]; then
	echo "manipulate proxy name in server.xml"
        xmlstarlet ed -i /Server/Service/Connector -t attr -n proxyName -v ${PROXY_NAME} ${BAMBOO_INSTALL}/conf/server.xml > /tmp/generated-server.xml
        mv /tmp/generated-server.xml ${BAMBOO_INSTALL}/conf/server.xml
fi

if [ "$VIRTUAL_HOST" != "" ]; then
	 if [ -f ${BAMBOO_HOME}/bamboo.cfg.xml ]; then
	 	xmlstarlet ed -u "/application-configuration/properties/property[@name='bamboo.jms.broker.client.uri']" -v "failover:(tcp://$VIRTUAL_HOST:54663?wireFormat.maxInactivityDuration=300000)?initialReconnectDelay=15000" ${BAMBOO_HOME}/bamboo.cfg.xml > /tmp/generated-bamboo.cfg.xml
	 	mv /tmp/generated-bamboo.cfg.xml ${BAMBOO_HOME}/bamboo.cfg.xml
	 fi
fi

if [ "$HTTPS" = "true" ]; then
	echo "enable https"
        xmlstarlet ed -i /Server/Service/Connector -t attr -n proxyPort -v 443 ${BAMBOO_INSTALL}/conf/server.xml > /tmp/generated-server.xml
        mv /tmp/generated-server.xml ${BAMBOO_INSTALL}/conf/server.xml
        xmlstarlet ed -i /Server/Service/Connector -t attr -n scheme -v https ${BAMBOO_INSTALL}/conf/server.xml > /tmp/generated-server.xml
        mv /tmp/generated-server.xml ${BAMBOO_INSTALL}/conf/server.xml
fi

if [ "$IMPORTCERT" = "true" ]; then 
	CATALINA_OPTS="-Djavax.net.ssl.trustStore=/opt/jdk/current/jre/lib/security/cacerts -Djavax.net.ssl.trustStorePassword=changeit $CATALINA_OPTS"      

	cd ${IMPORTPATH}
	if [ ! -f ${BAMBOO_INSTALL}/skipcert.conf ]; then
		for i in *
		do 
			/usr/lib/jvm/java-8-oracle/jre/bin/keytool -keystore /usr/lib/jvm/java-8-oracle/jre/lib/security/cacerts -importcert -alias $i -file $i -storepass changeit -noprompt
		done
		touch ${BAMBOO_INSTALL}/skipcert.conf
	fi
	cd ${BAMBOO_INSTALL}
fi

#if [ "$NEWRELIC" = "true" ]; then
#	unzip newrelic-java-${NEWRELIC_VERSION}.zip -d ${BAMBOO_INSTALL}
#
#	cd ${BAMBOO_INSTALL}/newrelic
#	java -jar newrelic.jar install
#	sed -ri "s/app_name:.*$/app_name: ${NEWRELIC_APP_NAME}/" newrelic.yml
#	sed -ri "s/license_key:[ \t]*'[^']+'/license_key: '${NEWRELIC_LICENSE}'/" newrelic.yml
#fi

#if [ "$WAIT" = "true" ]; then
#
#	is_ready() {
#    		eval "$WAIT_COMMAND"
#	}
#
#	# wait until is ready
#	i=0
#	while ! is_ready; do
#    		i=`expr $i + 1`
#    		if [ $i -ge ${WAIT_LOOPS} ]; then
#        		echo "$(date) - still not ready, giving up"
#        		exit 1
#    		fi
#    	echo "WAITING: $(date) - waiting to be ready for ${WAIT_COMMAND}"
#    	sleep ${WAIT_SLEEP}
#	done
#fi

${BAMBOO_INSTALL}/bin/start-bamboo.sh -fg
